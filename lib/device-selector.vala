/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

/*
 * ReactorConsole.DeviceSelector class can be used as a generic device selector
 * that manages an instance of Gtk.ComboBoxText widget. It looks for available
 * /dev/tty{USB,ACM}* device files and puts them as entries to the combo box.
 * Additionally it checks if the device is valid still. In case the device is no
 * longer available, it is removed. If there's only one device, it's selected
 * automatically. If there are more devices, it picks the first one.
 *
 * To create an instance of DeviceSelector, you have to create an instance of
 * Gtk.ComboBoxText widget and pass it as an argument. You should not manager
 * the widget on your own. The selected device path is available as a
 * "device-path" property. The selector works as long as "select-com" property
 * is set to true which is the default value.
 */

class ReactorConsole.DeviceUpdater {
    unowned ReactorConsole.DeviceSelector selector;
    uint source_id;

    public DeviceUpdater (DeviceSelector selector) {
		this.selector = selector;
		selector.update_device_list ();
		source_id = Timeout.add (1000, update);
    }

    ~DeviceUpdater () {
		Source.remove (source_id);
    }

    bool update () {
		selector.update_device_list ();
		return true;
    }
}

public class ReactorConsole.DeviceSelector : Object {
    public Gtk.ComboBoxText device_combo { get; construct; }
    public bool select_com {
		get {
		    return prop_select_com;
		}
		construct set {
		    if (value) {
				device_updater = new DeviceUpdater (this);
		    } else {
				device_updater = null;
		    }

		    prop_select_com = value;
		}
    }
    public string device_path { get; set; }

    bool prop_select_com;
    DeviceUpdater device_updater;

    public DeviceSelector (Gtk.ComboBoxText combo) {
		Object (device_combo: combo,
			select_com: true);

		device_path = device_combo.get_active_text ();

		device_combo.changed.connect (() => {
		    device_path = device_combo.get_active_text ();
		});

		update_device_list ();
    }

    ~DeviceSelector () {
		select_com = false;
		message ("destroy selector");
    }

    public void update_device_list () {
		const string[] prefixes = {
		    "ttyACM", "ttyUSB",
		};

		Dir dir;
		string[] prev = {};
		string[] found = {};

		try {
		    dir = Dir.open ("/dev", 0);
		} catch (Error e) {
		    error (e.message);
		}

		var filename = dir.read_name ();

		device_combo.get_model ().@foreach ((model, path, iter) => {
		    Value path_value;
		    model.get_value (iter, 0, out path_value);
		    prev += path_value.get_string ();
		    return false;
		});

		while (filename != null) {
		    foreach (var prefix in prefixes) {
				if (filename.has_prefix (prefix)) {
			    	found += "/dev/" + filename;
				}
		    }

		    filename = dir.read_name ();
		}

		bool changed = false;

		if (prev.length != found.length) {
		    changed = true;
		}

		if (!changed) {
		    Posix.qsort (prev, prev.length, sizeof (string),
				 (Posix.compar_fn_t) strcmp);
		    Posix.qsort (found, found.length, sizeof (string),
				 (Posix.compar_fn_t) strcmp);

		    for (int i = 0; i < found.length; i++) {
				if (prev[i] != found[i]) {
			    	changed = true;
			   		break;
				}
		    }
		}

		if (changed) {
		    var active_text = device_combo.get_active_text ();
		    var active_id = 0;

		    device_combo.set_active (-1);
		    device_combo.remove_all ();

		    for (int i = 0; i < found.length; i++) {
				device_combo.append_text (found[i]);

				if (found[i] == active_text) {
			    	active_id = i;
				}
		    }

		    device_combo.set_active (0);
		}
    }
}
