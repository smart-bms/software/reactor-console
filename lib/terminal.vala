/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

/*
 * ReactorConsole.Widget is the implementation of Reactor communication protocol
 * console. It allows the user to type the commands as a text and read the
 * device answer. It can be embed in any application that's based on Gtk+ 3.0
 * toolkit.
 *
 * "auto-scroll" property defines if the console should be scrolling down
 * automatically when there's new content to display. "com" property is the
 * current instance of the Reactor.Com object that's used to talk to the device.
 *
 * The logic behind the widget and command interpreter is implemented in
 * ReactorConsole.Console class.
 */

[GtkTemplate (ui = "/reactor/console/terminal.ui")]
public class ReactorConsole.Terminal : Gtk.Bin {
    [GtkChild]
    Gtk.ScrolledWindow console_scrolled;

    [GtkChild]
    Gtk.TextView console_view;

    [GtkChild]
    Gtk.Button scroll_button;

    [GtkChild]
    Gtk.CheckButton auto_scroll_check;

    [GtkChild]
    Gtk.Entry command_entry;

    [GtkChild]
    Gtk.Button send_button;

    public bool auto_scroll { get; set; }
    public Reactor.Com com { get; set; }

    Console console;

    public Terminal () {
		command_entry.activate.connect (execute_command);
		send_button.activate.connect (execute_command);
		scroll_button.clicked.connect (scroll_down);
		auto_scroll_check.bind_property ("active", this, "auto-scroll",
						 BindingFlags.SYNC_CREATE |
						 BindingFlags.BIDIRECTIONAL);
		console = new Console ();
		console.log_out.connect (log_out);
		console.log_in.connect (log_in);
		console.log_info.connect (log_info);

		bind_property ("com", console, "com",
			       BindingFlags.SYNC_CREATE);
    }

    public void scroll_down () {
		var adj = console_scrolled.get_vadjustment ();

		adj.set_value (adj.upper);
    }

    public void log_out (string message) {
		log_message (@"<-- $(message)");
    }

    public void log_in (string message) {
		log_message (@"--> $(message)");
    }

    public void log_info (string message) {
		log_message (@"### $(message)");
    }

    public void log_message (string message) {
		Gtk.TextIter iter;

		var buffer = console_view.get_buffer ();
		buffer.get_end_iter (out iter);
		buffer.insert (ref iter, message, -1);
		buffer.insert (ref iter, "\n", -1);

		if (auto_scroll) {
		    scroll_down ();
		}
    }

    void execute_command () {
		console.send_command (command_entry.get_text ());
		command_entry.set_text ("");
    }
}
