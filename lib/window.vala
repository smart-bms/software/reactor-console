/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

/*
 * ReactorConsole.Window widget can be used as a standalone window widget to
 * communicate with the Reactor device using ReactorConsole.Widget. It provides
 * the minimal implementation for selecting device and packs the console widget
 * as the content.
 */

[GtkTemplate (ui = "/reactor/console/window.ui")]
public class ReactorConsole.Window : Gtk.ApplicationWindow {
    public Reactor.Com com { get; construct; }
    public bool select_com { get; set; }

    [GtkChild]
    Gtk.ComboBoxText device_combo;
    
    [GtkChild]
    Gtk.Button reg_button;

    DeviceSelector device_selector;

    public Window (Gtk.Application app, bool select) {
		Object (application: app,
			select_com: select,
			com: new Reactor.ComTty (null));
	}
	
	construct {
		device_selector = new DeviceSelector (device_combo);
		device_selector.bind_property ("device-path", com, "device",
					       BindingFlags.SYNC_CREATE |
					       BindingFlags.BIDIRECTIONAL);

		reg_button.clicked.connect (() => new RegWindow ().show ());

		var terminal = new ReactorConsole.Terminal ();

		bind_property ("select-com", device_selector, "select-com",
			       BindingFlags.SYNC_CREATE);
		bind_property ("com", terminal, "com");
		bind_property ("select-com", device_combo, "visible",
			       BindingFlags.SYNC_CREATE);
		bind_property ("com", terminal, "com",
			       BindingFlags.SYNC_CREATE);
		add (terminal);
    }

    ~Window () {
		message ("destroy");
		select_com = false;
    }
}
