/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

/*
 * ReactorConsole.Console is the backend implementation of
 * ReactorConsole.Widget, yet it can be used separately to create for example a
 * command line tool. To work it needs Reactor.Com instance given as "com"
 * property. There are several methods that can be used to interact with the
 * device at higher level. Mostly send_command method that takes a string and
 * interprets it as a command. Command set is registered internally and can be
 * extended. Some commands may have several names, for example "query" can be
 * invoked using "qu" or "q" name as well. Commands may take some arguments
 * after the name separated with a space character.
 *
 * Console logging is emitted via log_* signals, where log_in stands for data
 * that comes to us, log_out for data that go to the device and log_info for the
 * local messages, like invalid argument warnings.
 */

public class ReactorConsole.Console : Object {
    public Reactor.Com com {
		get {
		    return com_prop;
		}
		set {
		    if (value != com_prop) {
			unbind_com ();
			com_prop = value;
			bind_com ();
		    }
		}
    }

    public signal void log_out (string message);
    public signal void log_in (string message);
    public signal void log_info (string message);

    delegate void CommandHandler (string[] args) throws Error;

    class CommandEntry : Object {
		public unowned CommandHandler handler;

		public CommandEntry (CommandHandler handler) {
		    this.handler = handler;
		}
    }

    Reactor.Com com_prop;
    ulong[] com_bind_ids;
    Gee.Map<string, CommandEntry> entry_map;

    construct {
		notify["com"].connect (bind_com);

		entry_map = new Gee.HashMap<string, CommandEntry> ();

		add_command (send_ping, "ping", "pg");
		add_command (send_query, "query", "qu", "q");
		add_command (send_timestamp, "timestamp", "ts");
		add_command (send_bq769x0, "bq769x0", "bq769", "bq");
    }

    void add_command (CommandHandler handler, ...) {
		var args = va_list ();
		var name = args.arg<string?> ();

		while (name != null) {
		    entry_map.@set (name, new CommandEntry (handler));
		    name = args.arg<string?> ();
		}
    }

    ~Console () {
		// TODO
		// unbind_com ();
    }

    public void send_command (string text) {
		return_if_fail (com != null);

		var split = text.split_set (" ");

		if (split.length < 1) {
		    return;
		}

		var entry = entry_map.@get (split[0]);

		if (entry != null) {
		    try {
				entry.handler (split[1:split.length]);
		    } catch (Error e) {
				warning (e.message);
		    }
		}
    }

    void send_ping (string[] args) throws Error {
		int ack;

		if (args.length > 0) {
		    ack = int.parse (args[0]).clamp (0, 255);
		} else {
		    ack = 1;
		}

		log_out (@"ping $(ack)");
		com.send_ping ((uint8) ack);
    }

    void send_query (string[] args) throws Error {
		Reactor.ComQuery flags = 0;

		foreach (var arg in args) {
		    arg = arg.down ();

		    if (arg.has_prefix ("v")) {
				flags |= Reactor.ComQuery.VOLTAGE;
		    } else if (arg.has_prefix ("c")) {
				flags |= Reactor.ComQuery.CURRENT;
		    } else {
				log_info (@"invalid query argument: $(arg)");
		    }
		}

		log_out ("query %04x".printf (flags));
		com.send_query (flags);
    }

    void send_timestamp (string[] args) throws Error {
		int64 ts;

		if (args.length < 1) {
		    ts = -1;
		} else {
		    ts = new DateTime.now_local ().to_unix ();
		}

		log_out (@"timestamp $(ts)");
		com.send_timestamp (ts);
    }

    void send_bq769x0 (string[] args) throws Error {
		if (args.length < 1) {
		    log_info ("missing action argument");
		}

		if (args.length < 2) {
		    log_info ("missing address argument");
		    return;
		}

		Reactor.ComBq769x0 action;
		uint8[] data;

		var addr = long.parse (args[1], 16);

		switch (args[0]) {
		    case "r":
				action = Reactor.ComBq769x0.READ;
				if (args.length < 3) {
				    data = { 1 };
				} else {
				    data = { (uint8) int.parse (args[2]) };
				}
			break;

		    case "w":
				action = Reactor.ComBq769x0.WRITE;
				if (args.length < 3) {
				    log_info ("no data to write");
				    return;
				} else {
				    data = new uint8[args.length - 2];

				    for (int i = 0; i < data.length; i++) {
						data[i] = (uint8) long.parse (args[i + 2], 16);
				    }
				}
			break;

		    default:
				log_info (@"invalid action: $(args[0])");
				return;
		}

		com.send_bq769x0 (action, (uint8) addr, data);
    }

    void bind_com () {
		unbind_com ();

		if (com == null) {
		    return;
		}

		ulong[] ids = {};

		ids += com.receive_ping.connect ((ack) => {
		    log_in (@"ping $(ack)");

		    if (ack > 0) {
				try {
				    send_ping ({ "%d".printf (ack - 1) });
				} catch (Error e) {
				    warning (e.message);
				}
		    }
		});

		ids += com.receive_voltage.connect ((values) => {
		    string msg = "";

		    foreach (var volt in values) {
				msg += "%d ".printf (volt);
		    }

		    log_in (msg);
		});

		ids += com.receive_current.connect ((current) => {
		    message ("receive current");
		    log_in (@"$(current)");
		});

		ids += com.receive_message.connect ((message) => {
		    log_in (message);
		});

		ids += com.receive_bq769x0.connect ((action, address, data) => {
		    if (action == Reactor.ComBq769x0.DATA) {
				string msg = "";

				foreach (var byte in data) {
				    msg += "%02x ".printf (byte);
				}

				log_in (msg);
		    }
		});

		com_bind_ids = ids;
    }

    void unbind_com () {
		if (com != null) {
		    foreach (var id in com_bind_ids) {
				com.disconnect (id);
		    }
		}

		com_bind_ids = null;
    }
}
