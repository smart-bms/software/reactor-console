namespace ReactorConsole.ChipModel {

    public struct Bit {
   
        public string name;
        public bool @value;
        public string[]? value_names;
        public bool writeable;
        public bool readable;
        
        public Bit (string name, string[]? value_names = null) {
            this.name = name;
            this.value_names = value_names;
            this.value = false;
            this.writeable = true;
            this.readable = true;
        }
        
        public Bit.unused () {
            this ("RSVD");
            this.readable = false;
            read_only ();
        }
        
        public Bit read_only () {
            this.writeable = false;
            return this;
        }
        
        public Gtk.Widget make_widget () {
            if (value_names != null) {
                var widget = new Gtk.ComboBoxText ();
                foreach (var vname in value_names) {
                    widget.append_text (name + ": " + vname);
                }
                widget.active = value? 1: 0;
                return widget;
            }
            
            var widget = new Gtk.CheckButton.with_label (name);
            widget.active = value;
            return widget;
        }
    }

    public class Register {
        public string name;
        public uint8 val = 0;
        public Bit[]? description;
        public bool write;
        
        public Register (string name, bool write = true) {
            this.name = name;
            this.write = write;
        }
        
        public Register.flags (string name, Bit[] description, bool write = true)
            requires (description.length == 8)
        {
            this (name);
            this.description = description;
        }
        
        public Gtk.Box make_widget () {
            var box = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 4);
            if (description != null) {
                foreach (var bit in description) {
                    box.pack_start (bit.make_widget ());
                }
            } else {
                var entry = new Gtk.Entry ();
                entry.text = val.to_string ();
                box.pack_start (entry);
            }
            return box;
        }
    }

    public abstract class Chip: Object {

        public abstract string name {get;}
        
        public abstract Register[]? registers {get;}

    }
}
