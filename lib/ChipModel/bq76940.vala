namespace ReactorConsole.ChipModel {

    public class BQ76940: Chip {

        public const string NAME = "BQ76940";
        
        public override string name {
            get {
                return NAME;
            }
        }
        
        static Bit[]? _SYS_STAT = null; // lazy evaluation
        static Bit[]? SYS_STAT {
            get {
                if (_SYS_STAT == null) {
                    _SYS_STAT = {
                        Bit("OCD"),
                        Bit("SCD"),
                        Bit("OV"),
                        Bit("UV"),
                        Bit("OVERD_ALERT"),
                        Bit("DEVICE_XREADY"),
                        Bit.unused(),
                        Bit("CC_READY"),
                    };
                }
                return _SYS_STAT;
            }
        }
        
        static Bit[]? _CELLBAL = null;
        static Bit[]? CELLBAL {
            get {
                if (_CELLBAL == null) {
                    _CELLBAL =  {
                        Bit("CB1"),
                        Bit("CB2"),
                        Bit("CB3"),
                        Bit("CB4"),
                        Bit("CB5"),
                        Bit.unused(),
                        Bit.unused(),
                        Bit.unused(),
                    };
                }
                return _CELLBAL;
            }
        }
        
        static Register[]? _registers = null;
        public override Register[]? registers {
            get {
                if (_registers == null) {
                    _registers = {
                        new Register.flags ("SYS_STAT", SYS_STAT),
                        new Register.flags ("CELLBAL1", CELLBAL),
                        new Register.flags ("CELLBAL2", CELLBAL),
                        new Register.flags ("CELLBAL3", CELLBAL),
                    };
                }
                return _registers;
            }
        }

    }
}
