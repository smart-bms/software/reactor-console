//[GtkTemplate (ui = "/reactor/console/registers.ui")]
public class ReactorConsole.Registers : Gtk.Grid {

    public Registers (ChipModel.Chip chip) {
        message (chip.name);
        var row = 0;
        foreach (var reg in chip.registers) {
            print (@"Register $(reg.name): ");
            this.attach (new Gtk.Label (reg.name), 0, row, 1, 1);
            var column = 2;
            foreach (var bit in reg.description) {
                this.attach (
                    bit.readable?
                        (Gtk.Widget) new Gtk.CheckButton.with_label (bit.name) :
                        (Gtk.Widget) new Gtk.Label (bit.name)
                    , column++, row, 1, 1);
                print (@"$(bit.name), ");
            }
            row++;
            print ("\n");
        }
        this.attach (new Gtk.Separator (Gtk.Orientation.VERTICAL), 1, 0, 1, chip.registers.length);
        this.column_spacing = 6;
        this.row_spacing = 6;
        this.margin = 12;
        this.show_all ();
    }

}
