[GtkTemplate (ui = "/reactor/console/reg_window.ui")]
public class ReactorConsole.RegWindow : Gtk.Window {
    
    [GtkChild]
    Gtk.ScrolledWindow scrolled_window;
    
    construct {
        var reg_list = new Registers (new ChipModel.BQ76940 ());
        reg_list.show ();
        scrolled_window.add (reg_list);
    }
    
}
