/*
 * Copyright (c) 2020 Reactor Energy
 * Mieszko Mazurek <mimaz@gmx.com>
 */

int main (string[] args)
{
    var app = new Gtk.Application ("com.energy.reactor.console",
				   ApplicationFlags.FLAGS_NONE);

    app.activate.connect (() => {
	new ReactorConsole.Window (app, true);
    });

    return app.run (args);
}
